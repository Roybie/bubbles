import animate;

import ui.resource.Image as Image;
import ui.View as View;
import ui.ImageView as ImageView;

import math.geom.Vec2D as Vec2D;

import src.utils.soundManager as soundManager;

var colours = [
    new Image({url: 'resources/images/bubble_white.png'}),
    new Image({url: 'resources/images/bubble_red.png'}),
    new Image({url: 'resources/images/bubble_yellow.png'}),
    new Image({url: 'resources/images/bubble_green.png'}),
    new Image({url: 'resources/images/bubble_blue.png'}),
    new Image({url: 'resources/images/bubble_purple.png'}),
];

var particles = [
    'resources/images/particle_white.png',
    'resources/images/particle_red.png',
    'resources/images/particle_yellow.png',
    'resources/images/particle_green.png',
    'resources/images/particle_blue.png',
    'resources/images/particle_purple.png',
];

exports = Class(View, function(supr) {
    this.init = function(opts) {
        opts = merge(opts, {
            visible: false,
        });

        supr(this, 'init', [opts]);

        //Used for checking removal and movement effects.
        this._connected = false;
        this._move_checked = false;
        this._distance = -1;
        this._row = -1;
        this._col = -1;

        this._velocity = new Vec2D({x: 0, y: 0});
        this._acceleration = new Vec2D({x: 0, y: 0});

        this.style.anchorX = this.style.width * 0.5;
        this.style.anchorY = this.style.height * 0.5;

        this.style.offsetX = -this.style.anchorX;
        this.style.offsetY = -this.style.anchorY;

        this._bubble_image = new ImageView({
            superview: this,
            x: 0,
            y: 0,
            width: this.style.width,
            height: this.style.height,
        });

        this.randomType();
    };

    //Give bubble a random type and chance to be a bomb
    this.randomType = function() {
        this._bubble_type = Math.floor(1 + Math.random() * (colours.length - 1));

        //5% chance of a bomb bubble
        if (Math.random() <= 0.05) {
            this._bubble_type = 0;
            this._bomb = true;
        } else {
            this._bomb = false;
        }

        this._bubble_image.setImage(colours[this._bubble_type]);
    }

    //Main loop
    this.tick = function(dt) {
        var dt = dt / 1000;
        //Move bubble if dropping
        if (this._velocity.getMagnitude() > 0) {
            if (this._dropping) {
                //Animate until bottom of screen
                this._velocity = this._velocity.add(this._acceleration.multiply(dt));
                this.style.x += this._velocity.x * dt;
                this.style.y += this._velocity.y * dt;

                if (this.style.y > 420) {
                    this._distance = 0;
                    this.pop();
                    this._dropping = false;
                    this._velocity = new Vec2D({x: 0, y: 0});
                }
            } else {
                //Firing particles
                var pe = this.getSuperview()._pop_particles,
                    data = pe.obtainParticleArray(1);
                var p = data[0];

                p.image = 'resources/images/particle_pipe.png';
                p.x = p.ox = this.style.x;
                p.y = p.oy = this.style.y;
                p.r = Math.random() * Math.PI * 2;
                p.theta = this._velocity.getAngle();

                p.width = p.height = 15;

                p.polar = true;
                p.dradius = 100;
                p.ddradius = -200;
                p.ttl = 300;
                pe.emitParticles(data);
            }
            //Bounce off walls
            if (this.style.x < this.getSuperview()._bubble_size / 2) {
                this.style.x = this.getSuperview()._bubble_size / 2;
                this._velocity.x *= -1;
            } else if (this.style.x > this.getSuperview().style.width - this.getSuperview()._bubble_size / 2) {
                this.style.x = this.getSuperview().style.width - this.getSuperview()._bubble_size / 2;
                this._velocity.x *= -1;
            }
        }
    };

    //Pop bubble
    this.pop = function() {
        //Pop animation, delayed slightly by distance
        animate(this).wait(this._distance * 50).then(function() {
            if (this._bomb) {
                soundManager.getSound().play('boom');
            } else {
                soundManager.getSound().play('pop');
            }
            var pe = this.getSuperview()._pop_particles,
                data = pe.obtainParticleArray(5);
            for (var i = 0; i < 5; i++) {
                var p = data[i];

                p.image = particles[this._bubble_type];
                p.x = p.ox = this.style.x;
                p.y = p.oy = this.style.y;
                p.r = p.theta = Math.random() * Math.PI * 2;

                p.width = p.height = 20;

                p.polar = true;
                p.dradius = 100;
                p.ddradius = -200;
                p.ttl = 300;
            }
            pe.emitParticles(data);
            if (this._bomb) {
                this.getSuperview().shake(this.style.x, this.style.y);
            }
            this.getSuperview()._bubble_pool.releaseView(this);
            //score of 10 * distance (bonus for long chains) + 100 bonus for dropped balls
            this.getSuperview().addScore(10 * this._distance + (this._dropping ? 100 : 0));
            //Reset values to be reused in the pool
            this.style.y = -50;
            this._connected = false;
            this._move_checked = false;
            this._distance = -1;
        });
    };

    //Drop the bubble then pop at the bottom of screen
    this.drop = function() {
        //Drop animation
        if (this._jolt_anim) {
            this._jolt_anim.clear();
        }
        if (this._shake_anim) {
            this._shake_anim.clear();
        }
        this.style.zIndex = 99;
        this._dropping = true;
        this._velocity.x = Math.random() * 300 - 150;
        this._velocity.y = - Math.random() * 200 - 200;
        this._acceleration.x = 0;
        this._acceleration.y = 750;
    };

    //Jolt when hit by fired bubble
    this.jolt = function(dir, dist) {
        if (this._move_anim) {
            return;
        }
        var amount = dist - this._distance;

        this._jolt_anim = animate(this).wait(this._distance * 50).then({x: this.style.x + (dir.x * amount), y: this.style.y + (dir.y * amount)}, 100, animate.easeOut).then({x: this.style.x, y: this.style.y}, 100, animate.easeOut);
    };

    //Shake when bomb explodes
    this.shake = function(amount, dir) {
        if (!this._shake_anim && !this._move_anim) {
            var speed = Math.random() * 40 + 180;
            this._shake_anim = animate(this)
                .now({x: this.style.x + dir.x * amount, y: this.style.y + dir.y * amount}, speed, animate.easeOut)
                .then({x: this.style.x - dir.x * amount, y: this.style.y - dir.y * amount}, speed * 2, animate.easeInOut)
                .then({x: this.style.x, y: this.style.y}, speed, animate.easeIn)
                .then(bind(this, function() { this._shake_anim = null; }));
        }
    };

    //Move to new position, optionally tweened
    this.moveTo = function(x, y, anim) {
        if (this._jolt_anim) {
            this._jolt_anim.clear();
        }
        if (this._shake_anim) {
            this._shake_anim.clear();
            this._shake_anim = null;
        }
        this._move_anim = null;
        if (anim) {
            this._move_anim = animate(this).now({x: x, y: y}, 400).then(function() { this._move_anim = null; });
        } else {
            this.style.x = x;
            this.style.y = y;
        }
    }
});
