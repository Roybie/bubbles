import ui.View as View;
import ui.TextView as TextView;
import ui.ViewPool as ViewPool;
import ui.ParticleEngine as ParticleEngine;

import math.geom.Vec2D as Vec2D;
import math.geom.Point as Point;

import src.entities.Bubble as Bubble;
import src.entities.Gun as Gun;
import src.utils.soundManager as soundManager;

var DT = 1 / 60,
    OK_BG = '#131'
    PINCH_BG = '#311';

exports = Class(View, function(supr) {
    this.init = function(opts) {
        supr(this, 'init', [opts]);

        //starting board dimensions
        this._cols = 10;
        this._rows = 10;

        this._bubble_size = 320 / this._cols,

        this._sound = soundManager.getSound();

        //Game over text view
        this._game_over_board = new TextView({
            superview: this,
            text: 'Game Over',
            color: '#FFF',
            backgroundColor: '#000',
            width: 300,
            height: 300,
            x: 10,
            y: 90,
            autoFontSize: false,
            size: 20,
            zIndex: 999,
            wrap: true,
        });

        //Score display
        this._score_board = new TextView({
            superview: this,
            text: '0',
            size: 30,
            color: '#FFF',
            x: 10,
            y: this.style.height - 40,
            width: 100,
            height: 30,
            autoSize: true,
            autoFontSize: false,
            zIndex: 9999,
        });

         //Gun and next bubble preview
        this._gun = new Gun({
            superview: this,
            x: this.style.width / 2,
            y: this.style.height,
            width: this._bubble_size * 1.25,
            height: this._bubble_size * 2.5,
            zIndex: 10,
        });

        //particle system for popping bubbles
        this._pop_particles = new ParticleEngine({
            superview: this,
            width: 100,
            height: 100,
            centerAnchor: true,
            zIndex: 100,
        });

        //bubble pool
        this._bubble_pool = new ViewPool({
            ctor: Bubble,
            initCount: 150,
            initOpts: {
                width: this._bubble_size,
                height: this._bubble_size,
            },
        });

        //set up when gaem start event is emitted
        this.getSuperview().on('gamescreen:start', bind(this, this.build));
    };

    //Set up game
    this.build = function() {
        this._game_started = false;
        this._game_over = false;
        this._can_restart = false;

        this._game_over_board.style.visible = false;

        this.style.backgroundColor = OK_BG;

        //used for constant timestep
        this._accumulated_time = 0;

        //used to add new rows periodically
        this._rows_accumulator = 0;
        this._rows_timeout = 20000;

        this._score = 0;
        this._score_board.setText(this._score);

        //TODO: un-hardcode sizes
        var y_offset = Math.sqrt(3) * 2/3;

        //Main bubbles data structure, 2D array
        this._board = [];

        //populate initial rows
        this.addRows(3);

        //Load the gun!
        var bubble = this._bubble_pool.obtainView();
        bubble.updateOpts({
            superview: this,
            x: this.style.width / 2,
            y: this.style.height - this._bubble_size / 2,
            visible: true,
            zIndex: 1,
        });
        var bubble2 = this._bubble_pool.obtainView();
        bubble2.updateOpts({
            superview: this,
            x: this.style.width / 2 + this._bubble_size * 2,
            y: this.style.height - this._bubble_size / 2,
            visible: true,
            zIndex: 1,
        });
        this._waiting_bubbles = [
            bubble,
            bubble2,
        ];

        //aim sets the positions of the waiting bubbles relative to the gun
        this.aim(new Vec2D({x: 0, y: -1}));

        //play background music
        this._sound.play('background');

        //game can start!
        this._game_started = true;

    };

    //Sets the bubble views to the correct positions based on their location in the array, called after new rows inserted etc
    this.setPositions = function(anim) {
        //y difference is less than x difference
        var y_offset = Math.sqrt(3) * 2/3;
        for (var row = 0; row < this._board.length; row++) {
            for (var col = 0; col < this._board[row].length; col++) {
                var bubble = this._board[row][col];

                if (bubble) {
                    bubble.moveTo(this._bubble_size / 2 + this._bubble_size * (col + 0.5 * (row&1)), this._bubble_size / 2 + this._bubble_size / y_offset * row, anim);
                }
            }
        }
    };

    //main loop
    this.tick = function(dt) {
        //Particle engine tick
        this._pop_particles.runTick(dt);

        //Don't need to do anything in these cases
        if (this._game_over || !this._game_started) {
            return;
        }

        //time accumulator for new rows
        this._rows_accumulator += dt;

        //if there is a firing bubble, move it and check collisions
        if (this._firing_bubble) {
            this._accumulated_time += ( dt / 1000);
            mainloop:
            while (this._accumulated_time >= DT) {
                this._accumulated_time -= DT;

                var dt = DT; //Math.min(dt / 1000, 0.05),
                bubble_size_squared = this._bubble_size * this._bubble_size * 0.8; //Give some leeway to allow squeezing through gaps

                this._firing_bubble.style.x += this._firing_bubble._velocity.x * dt;
                this._firing_bubble.style.y += this._firing_bubble._velocity.y * dt;

                //check for collisions with existing bubbles
                for (var row = this._board.length - 1; row >= 0; row--) {
                    for (var col = 0; col < this._board[row].length; col++) {
                        var bubble = this._board[row][col];
                        if (bubble) {
                            var bubble_pos = new Point(bubble.style.x, bubble.style.y),
                                firing_pos = new Point(this._firing_bubble.style.x, this._firing_bubble.style.y),
                                pos_difference = bubble_pos.subtract(firing_pos);
                            if (pos_difference.getSquaredMagnitude() < bubble_size_squared) {
                                //find co-ordinates for new position
                                var angle = pos_difference.getAngle(),
                                    angle_delta = Math.PI / 12,
                                    col_delta = row&1 ? 1 : 0,
                                    new_row = row,
                                    new_col = col;
                                //Right hand side
                                if (angle > Math.PI - angle_delta * 2 || angle <= -Math.PI + angle_delta) {
                                    this._board[row][col + 1] = this._firing_bubble;
                                    new_col++;
                                }
                                //Bottom Right
                                else if (angle > -Math.PI + angle_delta && angle <= -Math.PI / 2) {
                                    if (!this._board[row + 1]) {
                                        this._board[row + 1] = [];
                                    }
                                    this._board[row + 1][col + col_delta] = this._firing_bubble;
                                    new_row++;
                                    new_col += col_delta;
                                }
                                //Bottom Left
                                else if (angle > -Math.PI / 2 && angle <= -angle_delta) {
                                    if (!this._board[row + 1]) {
                                        this._board[row + 1] = [];
                                    }
                                    //Can't go off the left edge
                                    var column = Math.max(col - 1 + col_delta, 0);
                                    this._board[row + 1][column] = this._firing_bubble;
                                    new_row++;
                                    new_col = column;
                                }
                                //Left Side
                                else if (angle > -angle_delta && angle <= angle_delta * 2) {
                                    //Can't go off the left edge
                                    var column = Math.max(col - 1, 0);
                                    this._board[row][col - 1] = this._firing_bubble;
                                    new_col = column;
                                }
                                //Don't hit top side, introduces bug.
                                else {
                                    continue;
                                }

                                //save velocity for jolt animation
                                this._hit_velocity = this._firing_bubble._velocity.getUnitVector();
                                //reset velocity to stop trailing particles
                                this._firing_bubble._velocity = new Vec2D({x: 0, y: 0});
                                this._firing_bubble = null;
                                //Snap bubble to nearest position
                                this.setPositions();
                                //Test for matches
                                this.removeBubbles(new_row, new_col);
                                break mainloop;
                            }
                        }
                    }
                }

                //If there are no balls at the top level, stick to the first row.
                if (this._firing_bubble.style.y < this._bubble_size / 2) {
                    var x = this._firing_bubble.style.x;
                    var col = Math.floor(x / this._bubble_size);

                    this._board[0][col] = this._firing_bubble;
                    this._hit_velocity = this._firing_bubble._velocity.getUnitVector();
                    this._firing_bubble._velocity = new Vec2D({x: 0, y: 0});
                    this._firing_bubble = null;
                    //Snap bubble to nearest position
                    this.setPositions();
                    //Test for matches
                    this.removeBubbles(0, col);
                    break mainloop;
                }
            }
        } else {
            //prevents a build up of time and teleporting bubbles!
            this._accumulated_time = 0;

            //Add new rows if time elapsed is enough
            if (!this._popping && this._rows_accumulator >= this._rows_timeout) {
                this._rows_accumulator = 0;
                //reduce time for next additions, clamped to a minimum value
                this._rows_timeout = Math.max(7000, this._rows_timeout - 1000);

                this.addRows(1, true);
            }
        }

        //remove empty bottom rows
        if (this._board.length > 0 && this._board[this._board.length-1].every(function(b) { return !b; })) {
            this._board.pop();
        }

        //Add new rows if drops below 5
        if (!this._popping && this._board.length < 5) {
            //Reset the row adding accumulator so we don't get too many added at once
            this._rows_accumulator = 0;
            this.addRows(1, true); //Always add two at a time, so add 1 really ads two.
        }

        //play pinch tune if rows more than 12
        if (this._board.length >= 12 && !this._sound.isPaused('background')) {
            this.style.backgroundColor = PINCH_BG;
            this._bg_time = this._sound.getTime('background'); //Seems to just return 0 every time
            this._sound.pause('background');
            this._sound.play('pinch');
        } else if (this._board.length < 12 && this._sound.isPaused('background')) {
            this.style.backgroundColor = OK_BG;
            this._sound.pause('pinch');
            this._sound.play('background');
            this._sound.setTime('background', this._bg_time);
        }

        //If rows reaches 16, game over!
        if (this._board.length >= 16) {
            this._sound.stop('background');
            this._sound.stop('pinch');
            this._sound.play('death');
            this._game_over = true;

            this._game_over_board.style.visible = true;
            this._game_over_board.setText('Game Over\n\nScore: ' + this._score);

            //Drop bubbles in gun
            for (var i = 0; i < this._waiting_bubbles.length; i++) {
                var b = this._waiting_bubbles[i];
                if (b) {
                    b.drop();
                }
            }

            //drop board bubbles
            for (var i = 0; i < this._board.length; i++) {
                for (var j = 0; j < this._board[i].length; j++) {
                    var b = this._board[i][j];
                    if (b) {
                        setTimeout(bind(b, b.drop), 500);
                        this._board[i][j] = null;
                    }
                }
            }

            //stop game resetting until bubbles have dropped and popped!
            setTimeout(bind(this, function() {
                this._can_restart = true;
                this._game_over_board.setText(this._game_over_board.getText() + '\n\nClick to restart');
            }), 2000);
        }
    };

     //Add new rows to the main board, 2 at a time, because of coordinate system
    this.addRows = function(n, anim) {
        for (var row = n * 2 - 1; row >= 0; row--) {
            var bubble_row = []
            for (var col = 0; col < this._cols; col++) {
                //using offset coordinates means alternate rows are different, hence row&1
                if (col === this._cols - 1 && row&1 != 0) {
                    continue;
                }
                var bubble = this._bubble_pool.obtainView();
                bubble.updateOpts({
                    superview: this,
                    visible: true,
                });
                bubble.randomType();
                bubble_row[col] = bubble;
            }
            this._board.unshift(bubble_row);
        }
        this.setPositions(anim);
    };

     //Check if the given bubble is joined to 2 or more others of the same type and then remove if so
    this.removeBubbles = function(o_row, o_col) {
        var found_bubbles = [],
            bubbles_to_check = [],
            distance = 0;

        //set current row/col on all bubbles and found to false
        for (var i = 0; i < this._board.length; i++) {
            for (var j = 0; j < this._board[i].length; j++) {
                var b = this._board[i][j];
                if (b) {
                    b._row = i;
                    b._col = j;
                    b._found = false;
                }
            }
        }

        //set the fired bubbles distance and add to bubbles to check array
        this._board[o_row][o_col]._distance = distance;
        this._board[o_row][o_col]._found = true;
        bubbles_to_check.push(this._board[o_row][o_col]);

        //Neighbours:
        // BL: row + 1, col - (1 - row&1)
        // BR: row + 1, col + row&1
        // L:  row, col - 1
        // R:  row, col + 1
        // TL: row - 1, col - (1 - row&1)
        // TR: row - 1, col + row&1
        while (bubbles_to_check.length) {
            var next_bubbles = [];

            distance++;

            for (var i = 0; i < bubbles_to_check.length; i++) {
                var row = bubbles_to_check[i]._row,
                    col = bubbles_to_check[i]._col,
                    neighbours = [
                        [row+1, col-(1-(row&1))],
                        [row+1, col+(row&1)],
                        [row, col-1],
                        [row, col+1],
                        [row-1, col-(1-(row&1))],
                        [row-1, col+(row&1)],
                    ];
                //loop through bubbles neighbours and add same type (or bomb) bubbles to the array to check next loop
                neighbours.forEach(function(n) {
                    var bubble = this._board[n[0]] && this._board[n[0]][n[1]];
                    if (bubble) {
                        if (!bubble._found &&
                            (bubbles_to_check[i]._bomb ||
                            (bubble._bomb || bubble._bubble_type === bubbles_to_check[i]._bubble_type))) {
                            bubble._distance = distance;
                            bubble._found = true;
                            next_bubbles.push(bubble);
                        }
                    }
                }, this)
            }
            //add bubbles just checked to the found array and then update bubbles to check array with new bubbles
            found_bubbles = found_bubbles.concat(bubbles_to_check);
            bubbles_to_check = next_bubbles;
        }

        //If more than 3 bubbles, remove them
        if (found_bubbles.length >= 3) {
            var max_dist = 0;
            this._popping = true;
            this.jolt(o_row, o_col, this._hit_velocity, 6);
            found_bubbles.forEach(function(bubble) {
                bubble.pop();
                max_dist = Math.max(max_dist, bubble._distance);
                this._board[bubble._row][bubble._col] = null;
            }, this);

            this._drop_timeout = max_dist * 50 + 200;
            setTimeout(bind(this, function() { this._popping = false; }), this._drop_timeout + 300);

            //Check for disconnected bubbles only if bubbles have been removed
            var connected_bubbles = this._board[0].filter(function(b) { return b !== null; });

            while (connected_bubbles.length) {
                var next_bubbles = [];
                for (var i = 0; i < connected_bubbles.length; i++) {
                    var row = connected_bubbles[i]._row,
                        col = connected_bubbles[i]._col,
                        neighbours = [
                            [row+1, col-(1-(row&1))],
                            [row+1, col+(row&1)],
                            [row, col-1],
                            [row, col+1],
                            [row-1, col-(1-(row&1))],
                            [row-1, col+(row&1)],
                        ];

                    connected_bubbles[i]._connected = true;

                    neighbours.forEach(function(n) {
                        var bubble = this._board[n[0]] && this._board[n[0]][n[1]];
                        if (bubble && !bubble._connected) {
                            next_bubbles.push(bubble);
                        }
                    }, this);
                }
                connected_bubbles = next_bubbles;
            }
        } else {
            this.jolt(o_row, o_col, this._hit_velocity, 6);
        }

        //Reset all bubbles and drop disconnected ones
        for (var i = 0; i < this._board.length; i++) {
            for (var j = 0; j < this._board[i].length; j++) {
                var b = this._board[i][j];
                if (b) {
                    if (found_bubbles.length >= 3 && b._connected === false) {
                        setTimeout(bind(b, b.drop), this._drop_timeout);
                        this._board[i][j] = null;
                    } else {
                        b._move_checked = false;
                        b._connected = false;
                        b._distance = b._row = b._col = -1;
                    }
                }
            }
        }
    };

    //Shake all bubbles when bomb explodes
    this.shake = function(x, y) {
        for (var row = this._board.length - 1; row >= 0; row--) {
            for (var col = 0; col < this._board[row].length; col++) {
                var bubble = this._board[row][col];
                if (bubble) {
                    var dir = new Vec2D({x: bubble.style.x - x, y: bubble.style.y - y}).getUnitVector();
                    bubble.shake(4, dir);
                }
            }
        }
    };

    //jolt bubbles away from fired bubble
    this.jolt = function(row, col, dir, dist) {
        //Do momentum effect
        var new_bubble = this._board[row][col],
            move_bubbles = [],
            bubbles_to_move = [],
            distance = 0;

        if (new_bubble) {
            move_bubbles.push(new_bubble);
            new_bubble._distance = distance;
            new_bubble._move_checked = true;
        }

        while (distance < dist) {
            distance++;
            var next_bubbles = [];

            for (var i = 0; i < move_bubbles.length; i++) {
                var row = move_bubbles[i]._row,
                    col = move_bubbles[i]._col,
                    neighbours = [
                        [row+1, col-(1-(row&1))],
                        [row+1, col+(row&1)],
                        [row, col-1],
                        [row, col+1],
                        [row-1, col-(1-(row&1))],
                        [row-1, col+(row&1)],
                    ];

                neighbours.forEach(function(n) {
                    var bubble = this._board[n[0]] && this._board[n[0]][n[1]];
                    if (bubble && !bubble._move_checked) {
                        bubble._distance = distance;
                        bubble._move_checked = true;
                        next_bubbles.push(bubble);
                    }
                }, this);
            }
            bubbles_to_move = bubbles_to_move.concat(move_bubbles);
            move_bubbles = next_bubbles;
        }

        bubbles_to_move.forEach(function(b) {
            b.jolt(dir, dist);
        }, this);
    }

    //aim the gun
    this.aim = function(direction) {
        var offset = direction.multiply(this._bubble_size * 1.15);
        var offset2 = direction.multiply(this._bubble_size * 0.1);
        this._aiming_direction = direction;

        this._gun.style.r = direction.getAngle() + Math.PI / 2;

        this._waiting_bubbles[0].style.x = this._gun.style.x + offset.x;
        this._waiting_bubbles[0].style.y = this._gun.style.y + offset.y;

        this._waiting_bubbles[1].style.x = this._gun.style.x + offset2.x;
        this._waiting_bubbles[1].style.y = this._gun.style.y + offset2.y;
    };

    //Fire bubble
    this.fireBubble = function() {
        if (this._game_over || this._firing_bubble) {
            return;
        }

        this._sound.play('fire');
        this._firing_bubble = this._waiting_bubbles.shift();
        this._firing_bubble._velocity = this._aiming_direction.multiply(500);

        this._firing_bubble.style.zIndex = 101;

        this._waiting_bubbles[0].style.x = this.style.width / 2;
        var bubble = this._bubble_pool.obtainView();
        bubble.updateOpts({
            superview: this,
            x: this.style.width / 2 + this._bubble_size * 2,
            y: this.style.height - this._bubble_size / 2,
            zIndex: 1,
            visible: true,
        });
        bubble.randomType();
        this._waiting_bubbles.push(bubble);

        this.aim(this._aiming_direction);
    };

    //Update score
    this.addScore = function(score) {
        if (this._game_over) {
            return;
        }
        this._score += score;
        this._score_board.setText(this._score);
    };
});
