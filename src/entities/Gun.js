import ui.resource.Image as Image;
import ui.View as View;
import ui.ImageView as ImageView;

import src.entities.Bubble as Bubble;

var pipe_image = new Image({url: 'resources/images/pipe.png'});

exports = Class(View, function(supr) {
    this.init = function(opts) {
        supr(this, 'init', [opts]);

        this._bubble_size = opts.bubble_size;

        this.style.anchorX = this.style.width * 0.5;
        this.style.anchorY = this.style.height * 0.5;

        this.style.offsetX = -this.style.anchorX;
        this.style.offsetY = -this.style.anchorY;

        this._pipe_image = new ImageView({
            superview: this,
            image: pipe_image,
            x: 0,
            y: 0,
            width: this.style.width,
            height: this.style.height,
        });
    };
});

