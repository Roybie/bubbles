import ui.TextView;

exports = Class(ui.TextView, function(supr) {
    this.init = function(opts) {
        opts = merge(opts, {
            text: 'Click to Start',
            backgroundColor: '#00F',
            color: '#FFF',
            size: 20,
        });
        supr(this, 'init', [opts]);

        this.on('InputSelect', bind(this, function() {
            this.emit('titlescreen:start');
        }));
    }
});
