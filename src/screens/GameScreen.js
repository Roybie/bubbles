import ui.View;

import math.geom.Vec2D as Vec2D;

import src.entities.Board as Board;

exports = Class(ui.View, function(supr) {
    this.init = function(opts) {
        opts = merge(opts, {
            x: 0,
            y: 0,
            width: 320,
            height: 480,
        });

        supr(this, 'init', opts);

        this.buildLevel();

        this.style.backgroundColor = '#99999';
    };

    /*
     * Initialise game, board data structure etc
     */
    this.buildLevel = function() {
        this.style.width = 320;
        this.style.height = 480;

        /*
         * Make board store as 2 dimensional array
         */
        this._board = new Board({
            superview: this,
            width: this.style.width,
            height: this.style.height,
        });

        /*
         * Fire next bubble!
         */
        this.on('InputSelect', bind( this, function() {
            if (this._board._can_restart) {
                this.emit('gamescreen:gameover');
            } else {
                this._board.fireBubble();
            }
        }));

        /*
         * Aim next shot
         */
        this.on('InputMove', bind(this, function(evt, pt) {
            var direction = new Vec2D({x: pt.x - this.style.width / 2, y: pt.y - this.style.height}).getUnitVector();
            this._board.aim(direction);
        }));
    };

});
