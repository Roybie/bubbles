import AudioManager;

exports.sound = null;

exports.getSound = function() {
    if (!exports.sound) {
        exports.sound = new AudioManager({
            path: 'resources/audio',
            files: {
                pop: {
                    path: 'sfx',
                    background: false,
                },
                fire: {
                    path: 'sfx',
                    background: false,
                    volume: 0.5,
                },
                boom: {
                    path: 'sfx',
                    background: false,
                },
                death: {
                    path: 'sfx',
                    background: false,
                },
                background: {
                    path: 'music',
                    background: true,
                    volume: 0.3,
                    loop: true,
                },
                pinch: {
                    path: 'music',
                    background: true,
                    volume: 0.5,
                    loop: true,
                },
            }
        });
    }
    return exports.sound;
}
