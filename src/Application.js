import device;
import ui.StackView as StackView;

import src.screens.GameScreen as GameScreen;
import src.screens.TitleScreen as TitleScreen;

exports = Class(GC.Application, function () {

  this.initUI = function () {

    // Main gameplay screen
    var titleScreen = new TitleScreen();
    var gameScreen = new GameScreen();

    var scale = device.width / 320;

    //create a root view scaled to the screen
    var rootView = new StackView({
        superview: this,
        x: 0,
        y: (device.height - 480 * scale) / 2,
        width: 320,
        height: 480,
        clip: true,
        scale: scale,
    });

    rootView.push(titleScreen);

    titleScreen.on('titlescreen:start', function () {
        rootView.push(gameScreen);
        gameScreen.emit('gamescreen:start');
    });

    gameScreen.on('gamescreen:gameover', function() {
        rootView.pop();
    });

  };

  this.launchUI = function () {};

});
